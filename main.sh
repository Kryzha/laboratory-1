# Laboratory №1
pwd
mkdir schedule
cd schedule
mkdir week_1
mkdir week_2
mkdir week_1/monday
mkdir week_2/monday
touch week_1/monday/day_1.md
touch week_2/monday/day_1.md

ls week_1
ls week_2
ls week_1/monday
ls week_2/monday

cd /week_1/monday/
echo -e "
#Day_1
##10.25-12.00
Programming(practice)
##12.35-14.10
Programming(laboratory)
##14.30-16.05
English
">day_1.md
cat day_1.md

cd /week_2/monday/
echo -e "
#Day_1
##10.25-12.00
Programming(practice)
##12.35-14.10
Physic(laboratory)
##14.30-16.05
English
">day_1.md
cat day_1.md
